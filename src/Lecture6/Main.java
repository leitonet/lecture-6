package Lecture6;


public class Main {
    public static void main(String[] args) {
        String[] authors = {"sasa", "sdsd"};
        String[] articles = {"dddsss", "dddssrg"};
        Literature[] literatureArray = new Literature[3];

        literatureArray[0] = new Book("Voyna i Mir", "dgsuvjhsdhhhhhvjsk", 1875, "Moskov", authors);
        literatureArray[1] = new Journal("Za Rulem", "dsdsdsd", 2000, "Kharkov", articles);
        literatureArray[2] = new Directory("Gold Pages", "ddsgnmsdb", 1995, "nhd", " hdgf");


        //вывести в кнсоль информацию
        for (Literature literature : literatureArray) {
            System.out.println(literature.getInfo());
        }

        System.out.println();

        //действительно ли написана литература в 2000г.
        for (Literature literature : literatureArray) {
            if(literature.year == 2000){
                System.out.println("Литература 2000 года - " + literature.title);
            }
        }
    }
}
