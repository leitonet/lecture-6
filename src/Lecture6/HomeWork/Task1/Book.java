package Lecture6.HomeWork.Task1;


import java.util.Arrays;

public class Book extends Literature {

    String [] author;

    public Book(String title, String text, int year, String publisher, String [] author) {
        super(title, text, year, publisher);
        this.author = author;
    }

    @Override
    public String getInfo() {
        return "Book - " + super.getInfo() + Arrays.toString(author);
    }
}
