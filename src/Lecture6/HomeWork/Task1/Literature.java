package Lecture6.HomeWork.Task1;

public class Literature {
    String title;
    String text;
    int year;
    String publisher;

    public String getInfo(){
        return title + " " + text + " " + year + " " + publisher;
    }

    public Literature(String title, String text, int year, String publisher){
        this.title = title;
        this.text = text;
        this.year = year;
        this.publisher = publisher;
    }

}
