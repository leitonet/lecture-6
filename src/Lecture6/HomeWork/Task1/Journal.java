package Lecture6.HomeWork.Task1;

import java.util.Arrays;

public class Journal extends Literature {

    String[] articles;

    public Journal(String title, String text, int year, String publisher, String[] articles) {
        super(title, text, year, publisher);
        this.articles = articles;
    }

    @Override
    public String getInfo() {
        return "Journal - " + super.getInfo() + Arrays.toString(articles);
    }
}
