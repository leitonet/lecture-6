package Lecture6.HomeWork.Task1;

public class Directory extends Literature {

    String theme;

    public Directory(String title, String text, int year, String publisher, String theme) {
        super(title, text, year, publisher);
        this.theme = theme;
    }

    @Override
    public String getInfo() {
        return "Directory - " + super.getInfo() + theme;
    }
}
