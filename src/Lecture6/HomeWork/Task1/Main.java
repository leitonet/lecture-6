package Lecture6.HomeWork.Task1;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException{
        System.err.println("Сообщение 1");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] authors = {"sasa", "sdsd"};
        String[] articles = {"dddsss", "dddssrg"};
        Literature[] literatureArray = new Literature[3];

        literatureArray[0] = new Book("Voyna i Mir", "dgsuvjhsdhhhhhvjsk", 1875, "Moskov", authors);
        literatureArray[1] = new Journal("Za Rulem", "dsdsdsd", 2000, "Kharkov", articles);
        literatureArray[2] = new Directory("Gold Pages", "ddsgnmsdb", 1995, "nhd", " hdgf");


        //вывести в кнсоль информацию
        for (Literature literature : literatureArray) {
            System.out.println(literature.getInfo());
        }

        System.out.println();

        //действительно ли написана литература в данном интервале
        System.out.println("Введите количество лет, за которые нужно вывести литературу:");
        int howMatchyearsAgo = Integer.parseInt(reader.readLine());
        int nowYear = 2017;
        for (Literature literature : literatureArray) {
            if(literature.year >= nowYear - howMatchyearsAgo){
                System.out.println("Литература 2000 года - " + literature.title);
            }
        }
    }
}
