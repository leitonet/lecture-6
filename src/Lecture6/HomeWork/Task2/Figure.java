package Lecture6.HomeWork.Task2;

public class Figure {
    private String name;

    public Figure(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getSquare() {
        return 0;
    }
}
