package Lecture6.HomeWork.Task2;

public class JavaExampleOOP {

    public static void main(String[] args) {

        Figure[] listFigures = new Figure[4];
        listFigures[0] = new Triangle(3, 4, 5);
        listFigures[1] = new Rectangle(2, 5);
        listFigures[2] = new Foursquare(7);
        listFigures[3] = new Circle(4);


        //вариант с использованием полиморфизма
        for (Figure figure : listFigures) {
            double square = figure.getSquare();
            System.out.println(String.format("Square of %s = %.2f", figure.getName(), square));
        }

        //вариант с преобразованием типов
        for (Figure figure : listFigures) {

            //проверяем, является ли фигура треугольником...
            if (figure instanceof Triangle) {
                Triangle triangle = (Triangle) figure;
                double square = figure.getSquare();
                System.out.println(String.format("Square of %s = %.2f", triangle.getName(), square));

                //или прямоугольником...
            } else if (figure instanceof Rectangle) {
                Rectangle rectangle = (Rectangle) figure;
                double square = figure.getSquare();
                System.out.println(String.format("Square of %s = %.2f", rectangle.getName(), square));


                //или квадратом...
            } else if (figure instanceof Foursquare) {
                Foursquare foursquare = (Foursquare) figure;
                double square = figure.getSquare();
                System.out.println(String.format("Square of %s = %.2f", foursquare.getName(), square));


                //или вообще это круг...
            } else if (figure instanceof Circle) {
                Circle circle = (Circle) figure;
                double square = figure.getSquare();
                System.out.println(String.format("Square of %s = %.2f", circle.getName(), square));
            }
        }
    }

    static class Figure {

        private String name;

        public Figure(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public double getSquare() {
            return 0;
        }
    }

    static class Triangle extends Figure {

        //храним длины трех сторон
        private int[] sides;

        public Triangle(int firstSide, int secondSide, int thirdSide) {
            super("Triangle");
            sides = new int[3];
            sides[0] = firstSide;
            sides[1] = secondSide;
            sides[2] = thirdSide;
        }

        /**
         * Переопределяем метод, считаем площадь треугольника
         *
         * @return площадь треугольника
         */
        @Override
        public double getSquare() {

            //считаем полупериметр
            double p = (sides[0] + sides[1] + sides[2]) / 2;

            //считаем площать треугольника по формуле герона
            double square = Math.sqrt(p * (p - sides[0]) * (p - sides[1]) * (p - sides[2]));

            //возвращаем значение
            return square;
        }
    }

    static class Rectangle extends Figure {

        //храним только две стороны (ведь это прямоугольник, нам больше и не надо)
        private int[] sides;

        public Rectangle(int length, int width) {
            super("Rectangle");
            sides = new int[2];
            sides[0] = length;
            sides[1] = width;
        }

        @Override
        public double getSquare() {
            return sides[0] * sides[1];
        }
    }

    static class Foursquare extends Figure {

        //храним одну сторону, квадрат же
        int side;

        public Foursquare(int side) {
            super("Foursquare");
            this.side = side;
        }

        @Override
        public double getSquare() {
            return side * side;
        }
    }

    static class Circle extends Figure {

        //для круга нам нужен радиус
        int radius;

        public Circle(int radius) {
            super("Circle");
            this.radius = radius;
        }

        @Override
        public double getSquare() {
            return Math.PI * radius * radius;
        }
    }


}
