package Lecture6.HomeWork.Task2;

public class Polygon extends Figure {
    private int[] sides;

    public Polygon(String name) {
        super(name);
    }

    public Polygon(int countSides, int side) {
        super("Polygon");
        sides = new int[2];
        sides[0] = countSides;
        sides[1] = side;
    }

    @Override
    public double getSquare() {
        double square = sides.length * (Math.pow(sides[1], 2) / 4) * (1 / Math.tan(180 / sides.length));
        return square;
    }
}
