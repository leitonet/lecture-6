package Lecture6;


import java.util.Arrays;

public class Directory extends Literature{

    String theme;

    public Directory(String title, String text, int year, String publisher, String theme) {
        super(title, text, year, publisher);
        this.theme = theme;
    }

    @Override
    public String getInfo() {
        return "Directory - " + super.getInfo() + theme;
    }
}
